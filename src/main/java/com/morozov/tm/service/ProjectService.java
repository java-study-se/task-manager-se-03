package com.morozov.tm.service;

import com.morozov.tm.entity.Project;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    // отображение списка всех проектов
    public void showAllProject() {
        List<Project> projectList = projectRepository.findAll();
        if (projectList != null && !projectList.isEmpty()) {
            ConsoleHelper.writeString("Список проектов:");
            for (int i = 0; i < projectList.size(); i++) {
                ConsoleHelper.writeString(String.format("%d: %s", i, projectList.get(i).toString()));
            }
        } else {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }

    // добавление нового проекта
    public void addProject() {
        ConsoleHelper.writeString("Введите имя нового проекта");
        String projectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание нового проекта");
        String projectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала нового проекта в формате DD.MM.YYYY");
        Date startDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
        ConsoleHelper.writeString("Введите дату окончания нового проекта в формате DD.MM.YYYY");
        Date endDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
        Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(startDate);
        project.setEndDate(endDate);
        projectRepository.add(project);
        ConsoleHelper.writeString("Добавлен проект: " + project.toString());
    }

    // удаление проекта
    public String deleteProject() {
        String idDeletedProject = "";
        if (!projectRepository.findAll().isEmpty()) {
            ConsoleHelper.writeString("Введите порядковый номер проекта для удаления");
            int index = Integer.parseInt(ConsoleHelper.readString());
            if (checkIndex(index)) {
                idDeletedProject = projectRepository.deleteProjectWithReturnID(index);
                ConsoleHelper.writeString("Проект с порядковым номером " + index + " удален");
            }
        } else ConsoleHelper.writeString("Список проектов пуст");
        return idDeletedProject;
    }

    // обновление проекта
    public void updateProject() {
        if (!projectRepository.findAll().isEmpty()) {
            ConsoleHelper.writeString("Введите порядковый номер проекта для изменения");
            int index = Integer.parseInt(ConsoleHelper.readString());
            if (checkIndex(index)) {
                ConsoleHelper.writeString("Введите новое имя проекта");
                String projectName = ConsoleHelper.readString();
                ConsoleHelper.writeString("Введите новое описание проекта");
                String projectDescription = ConsoleHelper.readString();
                ConsoleHelper.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
                Date startDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
                ConsoleHelper.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
                Date endDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
                Project project = projectRepository.findByIndex(index);
                project.setName(projectName);
                project.setDescription(projectDescription);
                project.setStartDate(startDate);
                project.setEndDate(endDate);
                ConsoleHelper.writeString("Проект с порядковым номером " + index + " обновлен");
            } else {
                ConsoleHelper.writeString("Проекта с таким порядковым номером не существует");
            }
        } else {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }
    //очистка репозитория с проектами
    public void clearProjectList() {
        projectRepository.deleteAll();
        ConsoleHelper.writeString("Список проектов очищен");
    }

    //проверка существования введеного порядкового номера проекта
    public boolean checkIndex(int index) {
        if (projectRepository.findAll().size() > index && index >= 0) return true;
        else {
            ConsoleHelper.writeString("Проекта с данным порядковым номером не существует.");
            return false;
        }
    }
}
