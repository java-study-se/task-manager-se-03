package com.morozov.tm.service;

import com.morozov.tm.command.CommandEnum;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;

import java.util.*;

public class ApplicationStart {

    private static ProjectRepository projectRepository = new ProjectRepository();
    private static ProjectService projectService = new ProjectService(projectRepository);
    private static TaskRepository taskRepository = new TaskRepository();
    private static TaskService taskService = new TaskService(taskRepository);
        private Map<String, CommandEnum> commandMap;

    public ApplicationStart() {
        this.commandMap = ConsoleHelper.getFullCommandMap();
    }

    public static void startApp() {
        ApplicationStart start = new ApplicationStart();
        boolean isRunning = true;
        ConsoleHelper.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        while (isRunning) {
            String console = ConsoleHelper.readString();
            CommandEnum commandEnum = start.commandMap.get(console.toLowerCase());
            if (commandEnum != null) {
                switch (commandEnum) {
                    case HELP:
                        ConsoleHelper.showAllCommand();
                        break;
                    case PROJECT_CLEAR:
                        // сначала очишаем taskRepository от всех задач
                        taskService.clearTaskList();
                        // очищаем projectRepository от всех проектов
                        projectService.clearProjectList();
                        break;
                    case PROJECT_LIST:
                        projectService.showAllProject();
                        break;
                    case PROJECT_CREATE:
                        projectService.addProject();
                        break;
                    case PROJECT_REMOVE:
                        // получаем ID удаляемого проекта
                        String idDeletedProgect = projectService.deleteProject();
                        // передаем ID в метод для удаления всех задач по ID проекта
                        taskService.deleteAllTaskByProjectId(idDeletedProgect);
                        break;
                    case TASK_LIST:
                        taskService.showAllTask();
                        break;
                    case TASK_CLEAR:
                        taskService.clearTaskList();
                        break;
                    case TASK_CREATE:
                        taskService.addTask();
                        break;
                    case TASK_REMOVE:
                        taskService.deleteTask();
                        break;
                    case TASK_LIST_BY_PROJECT_ID:
                        taskService.getAllTaskByProjectId();
                    case EXIT:
                        isRunning = false;
                        break;
                }
            } else {
                ConsoleHelper.writeString("Команда не распознана, введите повторно. Для вызова справки введите \"help\"");
            }
        }
    }
}
