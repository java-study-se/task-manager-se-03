package com.morozov.tm.service;

import com.morozov.tm.entity.Task;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.List;


public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    //отображение списка всез задач
    public void showAllTask() {
        List<Task> taskList = taskRepository.findAll();
        if (!taskRepository.findAll().isEmpty()) {
            ConsoleHelper.writeString("Список задач:");
            for (int i = 0; i < taskList.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
            }
        } else {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }

    // добавление новой задачи
    public void addTask() {
        ConsoleHelper.writeString("Введите имя новой задачи");
        String taskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание новой задачи");
        String taskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала новой задачи в формате DD.MM.YYYY");
        Date startDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
        ConsoleHelper.writeString("Введите дату окончания новой задачи в формате DD.MM.YYYY");
        Date endDate = ConsoleHelper.formattedData(ConsoleHelper.readString());
        ConsoleHelper.writeString("Введите ID проекта задачи");
        String projectId = ConsoleHelper.readString();
        Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(startDate);
        task.setEndDate(endDate);
        task.setIdProject(projectId);
        ConsoleHelper.writeString("Добавлена задача" + task.toString());
    }

    //удаление задачи
    public void deleteTask() {
        if (!taskRepository.findAll().isEmpty()) {
            ConsoleHelper.writeString("Введите порядковый номер задачи для удаления");
            int index = Integer.parseInt(ConsoleHelper.readString());
            if (checkIndex(index)) {
                taskRepository.delete(index);
                ConsoleHelper.writeString("Задача с порядковым номером " + index + " удален");
            }
        } else {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }

    public void updateTask(int index, String name, String description, Date startDate, Date endDate, String projectId) {
        Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setEndDate(endDate);
        task.setIdProject(projectId);
        ConsoleHelper.writeString("Задача с порядковым номером " + index + " обновлена");
    }

    //все задачи по ID проекта
    public void getAllTaskByProjectId() {
        ConsoleHelper.writeString("Введите ID проекта");
        String projectId = ConsoleHelper.readString();
        List<Task> allTaskByProоectId = taskRepository.getAllTaskByProgectId(projectId);
        if (!allTaskByProоectId.isEmpty()) {
            ConsoleHelper.writeString("Список задач по проекту с ID: " + projectId);
            for (int i = 0; i < allTaskByProоectId.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, allTaskByProоectId.get(i).toString()));
            }
        }
    }

    //удаление всез задач по ID Проекта
    public void deleteAllTaskByProjectId(String projectId) {
        ConsoleHelper.writeString("Удаление задач с ID проекта: " + projectId);
        taskRepository.deleteAllTaskByProjectId(projectId);
    }
    //очистка репозитория с задачами
    public void clearTaskList() {
        taskRepository.deleteAll();
        ConsoleHelper.writeString("Список задач очищен");
    }

    //проверка существования введеного порядкового номера проекта
    public boolean checkIndex(int index) {
        if (taskRepository.findAll().size() < index) return true;
        else {
            ConsoleHelper.writeString("Проекта с данным порядковым номером не существует.");
            return false;
        }
    }
}
