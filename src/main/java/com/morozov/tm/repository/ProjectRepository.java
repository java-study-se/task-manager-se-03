package com.morozov.tm.repository;

import com.morozov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements Repository<Project> {
    private List<Project> projectList = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projectList;
    }

    @Override
    public Project findByIndex(int index) {
        return projectList.get(index);
    }

    @Override
    public void add(Project writeEntity) {
        projectList.add(writeEntity);
    }

    @Override
    public void update(int index, Project updateEntity) {
        projectList.add(index, updateEntity);
    }

    @Override
    public void delete(int index) {
        projectList.remove(index);
    }

    @Override
    public void deleteAll() {
        projectList.clear();
    }

    public String deleteProjectWithReturnID(int index) {
        String idDeletedProject = findByIndex(index).getId();
        delete(index);
        return idDeletedProject;
    }
}
