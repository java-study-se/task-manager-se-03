package com.morozov.tm.repository;

import com.morozov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements Repository<Task> {
    private List<Task> taskList = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return taskList;
    }

    @Override
    public Task findByIndex(int index) {
        return taskList.get(index);
    }

    @Override
    public void add(Task writeEntity) {
        taskList.add(writeEntity);
    }

    @Override
    public void update(int index, Task updateEntity) {
      taskList.add(index,updateEntity);
    }

    @Override
    public void delete(int index) {
      taskList.remove(index);
    }

    @Override
    public void deleteAll() {
        taskList.clear();
    }
    public List<Task> getAllTaskByProgectId(String projectId){
        List<Task> resultList = new ArrayList<>();
        for (Task task: taskList
             ) {
            if(task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }
    public void deleteAllTaskByProjectId(String projectId){
        List<Task> taskToDelete =getAllTaskByProgectId(projectId);
        for (Task task: taskToDelete
             ) {
            taskList.remove(task);
        }
    }

}
