package com.morozov.tm.repository;

import java.util.List;

public interface Repository<T> {

    List<T> findAll();

    T findByIndex(int id);

    void add(T writeEntity);

    void update(int id, T updateEntity);

    void delete(int id);

    void deleteAll();

}
