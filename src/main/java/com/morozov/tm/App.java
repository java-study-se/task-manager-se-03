package com.morozov.tm;

import com.morozov.tm.service.ApplicationStart;

public class App {
    public static void main(String[] args) {
        ApplicationStart.startApp();
    }
}
